﻿namespace NGitLab.Tests {
    public static class Config {
        public const string ServiceUrl = "https://gitlab.com/";
        public const string Secret = "!!!Replace with your personal access token!!!";

        public static GitLabClient Connect() {
            return GitLabClient.Connect(ServiceUrl, Secret, Impl.Api.ApiVersion.V4_OAuth, true);
        }
    }
}