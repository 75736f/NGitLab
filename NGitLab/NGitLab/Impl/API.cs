﻿using System;
using System.ComponentModel;
using System.Diagnostics;

namespace NGitLab.Impl {
    //[DebuggerStepThrough]
    public class Api {
        public enum ApiVersion
        {
            [Description("GitLab ApiV3")]
            V3,
            [Description("GitLab ApiV4")]
            V4,
            [Description("GitLab ApiV3 OAuth2")]
            V3_OAuth,
            [Description("GitLab ApiV4 OAuth2")]
            V4_OAuth
        }
        readonly string hostUrl;
        public readonly string ApiToken;
        internal     ApiVersion _ApiVersion { get; set; } = ApiVersion.V4_OAuth;
        public Api(string hostUrl, string apiToken, ApiVersion apiVersion, bool ignoreSslValidationErrors) :this(hostUrl,apiToken, ignoreSslValidationErrors)
        {
            _ApiVersion = apiVersion;
        }
        public Api(string hostUrl, string apiToken, bool ignoreSslValidationErrors) {
            this.hostUrl = hostUrl.EndsWith("/") ? hostUrl.Replace("/$", "") : hostUrl;
            ApiToken = apiToken;
            IgnoreSSLValidationErrors = ignoreSslValidationErrors;
        }

        private bool _IgnoreSSLValidationErrors;
        public bool IgnoreSSLValidationErrors
        {
            get
            {
                return _IgnoreSSLValidationErrors;
            }
            set
            {
                if(value != _IgnoreSSLValidationErrors)
                {
                    _IgnoreSSLValidationErrors = value;
                }
            }
        }

        public HttpRequestor Get() {
            return new HttpRequestor(this, MethodType.Get);
        }

        public HttpRequestor Post() {
            return new HttpRequestor(this, MethodType.Post);
        }

        public HttpRequestor Put() {
            return new HttpRequestor(this, MethodType.Put);
        }

        public HttpRequestor Delete() {
            return new HttpRequestor(this, MethodType.Delete);
        }

        public Uri GetApiUrl(string tailApiUrl) {
            if (!tailApiUrl.StartsWith("/"))
                tailApiUrl = "/" + tailApiUrl;
            return new Uri($"{hostUrl}{(hostUrl.EndsWith("/")?"":"/")}api/{(_ApiVersion== ApiVersion.V3?"v3":"v4")}{tailApiUrl}");
        }

        public Uri GetUrl(string tailApiUrl) {
            if (!tailApiUrl.StartsWith("/"))
                tailApiUrl = "/" + tailApiUrl;

            return new Uri(hostUrl + tailApiUrl);
        }
    }
}